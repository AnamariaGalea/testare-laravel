import unittest
import time
import random
import string
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


class TestWordpress(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('C:\\Users\\Anca\\PycharmProjects\\Tools\\chromedriver.exe')
        self.driver.get('https://wp.dev-society.com')

    def tearDown(self):
        self.driver.quit()

    def user_login(self, user, password):
        login_page = self.driver.find_element_by_xpath('//*[@id="menu-item-5"]/a')
        login_page.click()
        time.sleep(1)
        self.assertIn('wp-login', self.driver.current_url)

        email_box = self.driver.find_element_by_xpath('//*[@id="user_login"]')
        pass_box = self.driver.find_element_by_xpath('//*[@id="user_pass"]')

        email_box.clear()
        pass_box.clear()

        email_box.send_keys(user)
        pass_box.send_keys(password)

        login_button = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        login_button.click()
        time.sleep(2)

    def goto_PostsPage(self):
        posts_button = self.driver.find_element_by_xpath('//*[@id="menu-posts"]/a/div[3]')
        posts_button.click()
        time.sleep(1)
        self.assertIn('edit.php', self.driver.current_url)

    def goto_EmailPage(self):
        self.driver.get('https://getnada.com/')
        time.sleep(2)
        email = self.driver.find_element_by_xpath('//*[@id="app"]/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/h1/span[2]')
        email = email.text

        self.driver.get('https://wp.dev-society.com/')

        return email

    def test_ValidLogin(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        drop_down = self.driver.find_element_by_xpath('//*[@id="wp-admin-bar-my-account"]/a')

        hover = ActionChains(self.driver).move_to_element(drop_down)
        hover.perform()
        time.sleep(2)
        logout_button = self.driver.find_element_by_xpath('//*[@id="wp-admin-bar-logout"]/a')
        logout_button.click()

    def test_InvalidLogin(self):
        self.user_login('test_', 'cursuriazimut')
        self.assertIn('Invalid username', self.driver.page_source)

        self.driver.get('https://wp.dev-society.com')

        self.user_login('test_user', 'cursuriazi')
        self.assertIn('The password you entered for the username', self.driver.page_source)

        self.driver.get('https://wp.dev-society.com')

        self.user_login('', 'password')
        self.assertIn('The username field is empty.', self.driver.page_source)

        self.driver.get('https://wp.dev-society.com')

        self.user_login('test_user', '')
        self.assertIn('The password field is empty.', self.driver.page_source)

    def test_AddNewPost(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        self.goto_PostsPage()

        new_post = self.driver.find_element_by_xpath('//*[@id="wpbody-content"]/div[3]/a')
        new_post.click()
        time.sleep(1)
        self.assertIn('post-new.php', self.driver.current_url)

        post_title = self.driver.find_element_by_xpath('//*[@id="post-title-0"]')

        post_title.clear()

        post_title.send_keys('titlul postare')

        submit_button = self.driver.find_element_by_xpath('//*[@id="editor"]/div/div/div/div[1]/div[2]/button[2]')
        submit_button.click()
        time.sleep(1)
        submit_button = self.driver.find_element_by_xpath(
            '//*[@id="editor"]/div/div/div/div[3]/div/div/div[1]/div/button')
        submit_button.click()
        time.sleep(1)
        self.assertIn('is now live.', self.driver.page_source)

    def test_DeletePost(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        self.goto_PostsPage()

        first_post = self.driver.find_element_by_xpath('//*[@id="the-list"]/tr[1]/td[1]/strong')
        hover = ActionChains(self.driver).move_to_element(first_post)
        hover.perform()
        time.sleep(2)

        trash_post = self.driver.find_element_by_xpath('//*[@id="the-list"]/tr[1]/td[1]/div[3]/span[3]/a')
        trash_post.click()
        time.sleep(1)

        self.assertIn('1 post moved to the Trash.', self.driver.page_source)

    def test_RegisterUser(self):
        email = self.goto_EmailPage()
        register_button = self.driver.find_element_by_xpath('//*[@id="menu-item-6"]/a')
        register_button.click()
        time.sleep(2)
        self.assertIn('Register For This Site', self.driver.page_source)
        self.assertIn('wp-login.php?action=register', self.driver.current_url)

        random_username = ''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(random.randint(100000, 999999))
        password = 'ParolaSigura123'

        email_box = self.driver.find_element_by_xpath('//*[@id="user_email"]')
        username_box = self.driver.find_element_by_xpath('//*[@id="user_login"]')

        email_box.clear()
        username_box.clear()

        email_box.send_keys(email)
        username_box.send_keys(random_username)

        register_button = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        register_button.click()

        time.sleep(2)
        self.assertIn('Registration complete. Please check your email.', self.driver.page_source)
        self.assertIn('wp-login.php?checkemail=registered', self.driver.current_url)

        time.sleep(10)
        self.driver.get('https://getnada.com/')

        time.sleep(5)

        message = self.driver.find_element_by_xpath('//*[@id="app"]/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div/div[1]/ul/li/div/div[1]/div[1]')
        message.click()

        time.sleep(5)
        self.driver.switch_to.frame(self.driver.find_element_by_id('idIframe'))
        time.sleep(1)
        link = self.driver.find_element_by_tag_name('https:')
        link = link.get_attribute('wp-login.php?action')
        link = 'https://wp.dev-society.com/wp-login.php?action=' + link
        print(link)
        time.sleep(1)
        self.driver.switch_to.default_content()
        self.driver.get(link)

        time.sleep(1)

        new_password = self.driver.find_element_by_xpath('//*[@id="pass1-text"]')
        new_password.clear()
        new_password.send_keys(password)

        submit = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        submit.click()

        time.sleep(10)

        self.assertIn('Your password has been reset', self.driver.page_source)

        self.driver.get('https://wp.dev-society.com')
        time.sleep(1)
        self.user_login(random_username, password)
        self.assertIn('wp-admin', self.driver.current_url)

    def test_AddNewComment(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        home_page = self.driver.find_element_by_xpath('//*[@id="wp-admin-bar-site-name"]/a')
        home_page.click()

        time.sleep(1)

        first_post = self.driver.find_element_by_xpath('//*[@id="recent-posts-2"]/ul/li[1]/a')
        first_post.click()

        self.assertIn('post comment', self.driver.page_source.lower())
        #.lower-> face ce gaseste in page source in litere mici. asa o sa gasesca 'post comment' in continutul paginii noastre unde era "POST COMMENT"

        comment_box = self.driver.find_element_by_xpath('//*[@id="comment"]')
        comment_box.send_keys('asdfghjasdf')

        post_button = self.driver.find_element_by_xpath('//*[@id="submit"]')
        post_button.click()

        self.assertIn('comment', self.driver.current_url)

    def test_DeleteComment(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        posts = self.driver.find_element_by_xpath('//*[@id="menu-comments"]/a/div[3]')
        posts.click()

        first_comment = self.driver.find_element_by_xpath('//*[@id="the-comment-list"]/tr[1]/td[2]/p')

        hover = ActionChains(self.driver).move_to_element(first_comment)
        hover.perform()

        trash_button = self.driver.find_element_by_xpath('//*[@id="the-comment-list"]/tr[1]/td[2]/div[3]/span[7]/a')
        trash_button.click()




