import unittest
from selenium import webdriver
import time
import sys
from selenium.webdriver.common.action_chains import ActionChains

class TestareLaravel (unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('C:\\Users\\Anca\\PycharmProjects\\Tools\\chromedriver.exe')
        self.driver.get ('https://laravel.dev-society.com/')

    def tearDown(self):
        self.driver.quit()

    def HomePage(self):
        about_us_button = self.driver.find_element_by_xpath('//*[@id="about_page"]')
        about_us_button.click()
        self.assertIn('about', self.driver.current_url)
        self.assertIn('About', self.driver.page_source)

    def Register(self, nume, email, parola, confirmare_parola):
        register_button = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()
        self.assertIn('register', self.driver.current_url)
        time.sleep(2)

        name_box = self.driver.find_element_by_xpath('//*[@id="name"]')
        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        parola_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        confirmare_parola_box = self.driver.find_element_by_xpath('//*[@id="password-confirm"]')

        name_box.clear()
        email_box.clear()
        parola_box.clear()
        confirmare_parola_box.clear()
        name_box.send_keys(nume)
        email_box.send_keys(email)
        parola_box.send_keys(parola)
        confirmare_parola_box.send_keys(confirmare_parola)

        register_button2 = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        register_button2.click()

    def ValidLogin(self, email, parola):
        login_button= self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login_button.click()

        self.assertIn('login', self.driver.current_url)
        time.sleep(2)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.clear()

        password_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        password_box.clear()

        email_box.send_keys(email)
        password_box.send_keys(parola)

        login_button2 = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        login_button2.click()

    def RegisterTest(self):
        self.Register('Adela3', 'adela3@test.azimut', 'Azimut2016', 'Azimut2016')

        self.assertIn('dashboard', self.driver.current_url)
        self.assertIn('Welcome,', self.driver.page_source)

    def InvalidRegister_noName(self):
        self.Register('', 'anamaria@test.com', 'azimut', 'azimut')
        self.assertIn('register', self.driver.current_url)

    def ValidLoginTest(self):
        login_button= self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login_button.click()

        self.assertIn('login', self.driver.current_url)
        time.sleep(2)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.clear()

        password_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        password_box.clear()

        email_box.send_keys('anamaria@test.com')
        password_box.send_keys('azimut')

        login_button2 = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        login_button2.click()

        self.assertIn('dashboard', self.driver.current_url)

    def LogIn_InvalidPaswword(self):
        self.ValidLogin('anamaria@test.com', 'mama')
        self.assertIn('These credentials do not match our records', self.driver.page_source)

    def LogIn_InvalidEmail(self):
        self.ValidLogin('dfghj@asdf', 'azimut')
        self.assertIn('These credentials do not match our records', self.driver.page_source)

    def AddNewPost(self):
        self.ValidLogin('anamaria@test.com', 'azimut')
        posts_button = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        posts_button.click()

        add_post_button = self.driver.find_element_by_xpath('//*[@id="create_post"]')
        add_post_button.click()

        post_title = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        post_title.send_keys('Prima mea postare pe Laravel')

        post_body = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        post_body.send_keys('Sper sa mearga bine :)')

        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()

        self.assertIn('posts', self.driver.current_url)
    #variabila globala:
        TestareLaravel.id_post = self.driver.current_url.split('/')[-1]

    def Editpost(self):
        self.ValidLogin('anamaria@test.com', 'azimut')
        edit_button = self.driver.find_element_by_xpath('//*[@id="edit-post-' + TestareLaravel.id_post + '"]')
        edit_button.click()

        post_title_edit = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        post_title_edit.clear()
        post_title_edit.send_keys('Prima mea postare este un succes')

        post_body_edit = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        post_body_edit.send_keys('\n editare postare')

        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()
        self.assertIn('updated', self.driver.page_source)
        time.sleep(2)

    def DeletePost(self):
        self.ValidLogin('anamaria@test.com', 'azimut')
        delete_button= self.driver.find_element_by_xpath('//*[@id="delete-post-' + TestareLaravel.id_post+'"]')
        delete_button.click()
        self.assertIn('Post deleted', self.driver.page_source)
        time.sleep(2)

    def EditUser(self):
        self.ValidLogin('adela3@test.azimut', 'Azimut2016')
        settings_button = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        time.sleep(2)
        self.assertIn('settings', self.driver.current_url)

        edit_button = self.driver.find_element_by_xpath('//*[@id="edit_account"]')
        edit_button.click()

        account_type= self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/select')
        account_type.click()

        admin_option = self.driver.find_element_by_xpath('//*[@id="admin"]')
        admin_option.click()
        time.sleep(2)

        self.assertIn('admin', self.driver.page_source)


    def DeleteUser(self):
        self.ValidLogin('adela3@test.azimut', 'Azimut2016')
        settings_button = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        time.sleep(2)
        self.assertIn('settings', self.driver.current_url)

        delete_button = self.driver.find_element_by_xpath('//*[@id="delete_button"]')
        delete_button.click()

def main(test_plan):
    test_suite = unittest.TestSuite()

    #test_suite.addTest(TestareLaravel('test_Register'))
    #test_suite.addTest(TestareLaravel('test_DeleteUser'))

    file = open(test_plan, 'r')
    lines = file.read().split('\n')
    for line in lines:
        test_suite.addTest(TestareLaravel(line))

    file.close()

    runner = unittest.TextTestRunner(verbosity=3)
    runner.run(test_suite)

main(sys.argv[1])