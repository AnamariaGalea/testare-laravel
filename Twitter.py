import unittest
from selenium import webdriver
import time

class TestTwitter(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('C:\\Users\\Anca\\PycharmProjects\\Tools\\chromedriver.exe')
        self.driver.get('https://twitter.com/')

    def tearDown(self):
        self.driver.quit()

    def test_LogIn(self):
        login_button = self.driver.find_element_by_xpath('//*[@id="doc"]/div/div[1]/div[1]/div[2]/div[2]/div/a[2]')
        login_button.click()
        time.sleep(2)
        self.assertIn('login', self.driver.current_url)

        email = self.driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[1]/input')
        email.clear()
        email.send_keys('george@azimutvision.ro')

        password = self.driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[2]/input')
        password.clear()
        password.send_keys('cursurizimut')

        login_button2= self.driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/div[2]/button')
        login_button2.click()

        self.assertIn('Home', self.driver.page_source)

