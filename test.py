import requests

def foo2():
    response = requests.get('https://reqres.in/api/users/3')

    print('I Was ' + response.json()['data']['last_name'])

def foo(a):
    if a == 0:
        return 0
    else:
        return foo(a-1) + 100
print(foo(5))