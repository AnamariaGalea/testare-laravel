import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import unittest


class TestWordPress(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('C:\\Users\\Anca\\PycharmProjects\\Tools\\chromedriver.exe')
        self.driver.get('https://wp.dev-society.com')

    def tearDown(self):
        self.driver.quit()

    def user_login(self, user,password):
        logIn_page = self.driver.find_element_by_xpath ('//*[@id="menu-item-5"]/a')
        logIn_page.click()
        time.sleep(1)
        self.assertIn('wp-login', self.driver.current_url)

        email_box = self.driver.find_element_by_xpath ('//*[@id="user_login"]')
        pass_box = self.driver.find_element_by_xpath ('//*[@id="user_pass"]')

        email_box.clear()
        pass_box.clear()

        email_box.send_keys(user)
        pass_box.send_keys(password)

        login_button = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        login_button.click()
        time.sleep(2)

    def gotoPosts(self):
        post_button = self.driver.find_element_by_xpath('//*[@id="menu-posts"]/a/div[3]')
        post_button.click()
        time.sleep(3)
        self.assertIn('edit.php', self.driver.current_url)

    def test_ValidLogin(self):
        self.user_login ('test_user', 'cursuriazimut')
        self.assertIn ('wp-admin', self.driver.current_url)

        drop_down = self.driver.find_element_by_xpath('//*[@id="wp-admin-bar-my-account"]/a')

        hover = ActionChains(self.driver).move_to_element(drop_down)
        hover.perform()
        time.sleep(5)

        logout_button = self.driver.find_element_by_xpath('//*[@id="wp-admin-bar-logout"]/a')
        logout_button.click()

    def test_InvalidUserID(self):
        self.user_login('user gresit', 'cursuriazimut')
        self.assertIn('Invalid username', self.driver.page_source)

    def test_InvalidPassword(self):
        self.user_login('test_user', 'cursuri')
        self.assertIn('The password you entered for the username', self.driver.page_source)

    def test_NoUserID(self):
        self.user_login('', 'cursuriazimut')
        self.assertIn('The username field is empty', self.driver.page_source)

    def test_NoPassword(self):
        self.user_login('test_user', '')
        self.assertIn('The password field is empty', self.driver.page_source)

    def test_AddNewpost(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        post_button = self.driver.find_element_by_xpath('//*[@id="menu-posts"]/a/div[3]')
        post_button.click()
        time.sleep(3)
        self.assertIn('edit.php', self.driver.current_url)

        add_new_button = self.driver.find_element_by_xpath('//*[@id="wpbody-content"]/div[3]/a')
        add_new_button.click()
        time.sleep(3)
        self.assertIn('post-new',self.driver.current_url)

        post_title = self.driver.find_element_by_xpath('//*[@id="post-title-0"]')
        post_title.clear()
        post_title.send_keys('Home test')
        time.sleep(3)

        publish_button = self.driver.find_element_by_xpath('//*[@id="editor"]/div/div/div/div[1]/div[2]/button[2]')
        publish_button.click()

        publish_button2 = self.driver.find_element_by_xpath('//*[@id="editor"]/div/div/div/div[3]/div/div/div[1]/div/button')
        publish_button2.click()

        time.sleep(5)
        self.assertIn('is now live', self.driver.page_source)

    def test_DeletePost(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)
        self.gotoPosts()

        first_post = self.driver.find_element_by_xpath('//*[@id="post-203"]/td[1]/strong')
        hover = ActionChains(self.driver).move_to_element(first_post)
        hover.perform()
        time.sleep(3)

        trash_button = self.driver.find_element_by_xpath('//*[@id="post-203"]/td[1]/div[3]/span[3]/a')
        trash_button.click()

        self.assertIn('1 post moved to the Trash', self.driver.page_source)
