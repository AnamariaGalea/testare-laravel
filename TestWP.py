import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import random
import string

#clasa de mai jos mosteneste unittest.TestCase
class TestWordPress(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('C:\\Users\\Anca\\PycharmProjects\\Tools\\chromedriver.exe')
        self.driver.get ('https://wp.dev-society.com')
    def tearDown(self):
        self.driver.quit()

    def user_login(self, user, password):
        login_page = self.driver.find_element_by_xpath('//*[@id="menu-item-5"]/a')
        login_page.click()
        time.sleep(1)
        self.assertIn('wp-login', self.driver.current_url)

        email_box = self.driver.find_element_by_xpath('//*[@id="user_login"]')
        pass_box = self.driver.find_element_by_xpath('//*[@id="user_pass"]')

        email_box.clear()
        pass_box.clear()

        email_box.send_keys(user)
        pass_box.send_keys(password)

        login_button = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        login_button.click()
        time.sleep(2)

    def register(self, userRegister, emailRegister):
        register_button = self.driver.find_element_by_xpath('//*[@id="menu-item-6"]/a')
        register_button.click()
        self.assertIn('wp-login', self.driver.page_source)

        user_name_box = self.driver.find_element_by_xpath('//*[@id="user_login"]')
        email_box = self.driver.find_element_by_xpath('//*[@id="user_email"]')

        user_name_box.clear()
        email_box.clear()

        user_name_box.send_keys(userRegister)
        email_box.send_keys(emailRegister)

        register_button = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        register_button.click()
        time.sleep(10)

    def goto_PostPage(self):
        posts_button = self.driver.find_element_by_xpath('//*[@id="menu-posts"]/a/div[3]')
        posts_button.click()
        time.sleep(1)
        self.assertIn('edit.php', self.driver.current_url)

    def goto_EmailPage(self):
        self.driver.get('https://getnada.com/')
        time.sleep(2)
        email = self.driver.find_element_by_xpath('//*[@id="app"]/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/h1/span[2]')
        email = email.text

        self.driver.get('https://wp.dev-society.com/')

        return email

    def test_ValidLogin(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        drop_down = self.driver.find_element_by_xpath('//*[@id="wp-admin-bar-my-account"]/a')

        hover = ActionChains(self.driver).move_to_element(drop_down)
        hover.perform()
        time.sleep(2)
        logout_button = self.driver.find_element_by_xpath('//*[@id="wp-admin-bar-logout"]/a')
        logout_button.click()

    def test_invalidLogin(self):
        #user gresit
        self.user_login('test_', 'cursuriazimut')
        self.assertIn('Invalid username', self.driver.page_source)

        self.driver.get('https://wp.dev-society.com')

        #parola gresita
        self.user_login('test_user', 'cursuriazi')
        self.assertIn('The password you entered for the username', self.driver.page_source)

        self.driver.get('https://wp.dev-society.com')

        #username empty
        self.user_login('', 'password')
        self.assertIn('The username field is empty', self.driver.page_source)

        self.driver.get('https://wp.dev-society.com')

        #parola empty
        self.user_login('test_user', '')
        self.assertIn('The password field is empty', self.driver.page_source)

    def test_AddNewPost(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        posts_button = self.driver.find_element_by_xpath('//*[@id="menu-posts"]/a/div[3]')
        posts_button.click()
        time.sleep(1)
        self.assertIn('edit.php', self.driver.current_url)

        add_new_button = self.driver.find_element_by_xpath('//*[@id="wpbody-content"]/div[3]/a')
        add_new_button.click()
        time.sleep(1)
        self.assertIn('post-new', self.driver.current_url)

        post_title = self.driver.find_element_by_xpath('//*[@id="post-title-0"]')
        post_title.clear()
        post_title.send_keys('Uhuuuu')
        time.sleep(3)

        submit_button = self.driver.find_element_by_xpath('//*[@id="editor"]/div/div/div/div[1]/div[2]/button[2]')
        submit_button.click()

        submit_button2= self.driver.find_element_by_xpath('//*[@id="editor"]/div/div/div/div[3]/div/div/div[1]/div/button')
        submit_button2.click()
        time.sleep(1)
        self.assertIn('Post published', self.driver.page_source)

    def test_DeletePost(self):
        self.user_login('test_user', 'cursuriazimut')
        self.assertIn('wp-admin', self.driver.current_url)

        self.goto_PostPage()

        first_post = self.driver.find_element_by_xpath('//*[@id="the-list"]/tr[1]/td[1]/strong')
        hover = ActionChains(self.driver).move_to_element(first_post)
        hover.perform()
        time.sleep(10)

        trash_post = self.driver.find_element_by_xpath('//*[@id="the-list"]/tr[1]/td[1]/div[3]/span[3]/a')
        trash_post.click()
        time.sleep(10)

        self.assertIn('1 post moved to the Trash', self.driver.page_source)

    '''def test_Register(self):
        register_button = self.driver.find_element_by_xpath('//*[@id="menu-item-6"]/a')
        register_button.click()
        time.sleep(2)
        self.assertIn('wp-login', self.driver.page_source)
        self.assertIn('Register For This Site', self.driver.page_source)

        user_name_box = self.driver.find_element_by_xpath('//*[@id="user_login"]')
        email_box = self.driver.find_element_by_xpath('//*[@id="user_email"]')

        user_name_box.clear()
        email_box.clear()

        #user_name_box.send_keys(random.randint(100000, 999999))
        user_name_box.send_keys(''.join(random.choices(string.ascii_lowercase,k=10))+ '_'+ str(random.randint(100000, 999999)))
        puteam sa definim o var random_username si random_email:
        random_username= ''.join(random.choices(string.ascii_lowercase,k=10))+ '_'+ str(random.randint(100000, 999999)))
        random_email = ''.join(random.choices(random_characters, k=7)) + '@domain.com')
        user_nama_box.send.keys(random_username)
        email_box.send_keys(random_email)
        '''

        random_characters = string.ascii_letters + string.digits
        #email_box.send_keys(''.join(random.choice(random_characters) for _ in range (7)) + '@domain.com')
        email_box.send_keys(''.join(random.choices(random_characters, k=7)) + '@domain.com')

        register_button = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        register_button.click()

        time.sleep(2)
        self.assertIn('Registration complete.', self.driver.page_source)
        self.assertIn('=registered', self.driver.current_url)

    def test_ExistingUser(self):
        self.register('test_user', 'fghgfh')
        self.assertIn('This username is already registered', self.driver.page_source)'''


    def test_RegisterUser(self):
        email = self.goto_EmailPage()
        register_button = self.driver.find_element_by_xpath('//*[@id="menu-item-6"]/a')
        register_button.click()
        time.sleep(2)
        self.assertIn('Register For This Site', self.driver.page_source)
        self.assertIn('wp-login.php?action=register', self.driver.current_url)

        random_username = ''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(random.randint(100000, 999999))

        email_box = self.driver.find_element_by_xpath('//*[@id="user_email"]')
        username_box = self.driver.find_element_by_xpath('//*[@id="user_login"]')

        email_box.clear()
        username_box.clear()

        email_box.send_keys(email)
        username_box.send_keys(random_username)

        register_button = self.driver.find_element_by_xpath('//*[@id="wp-submit"]')
        register_button.click()

        time.sleep(2)
        self.assertIn('Registration complete. Please check your email.', self.driver.page_source)
        self.assertIn('wp-login.php?checkemail=registered', self.driver.current_url)

        time.sleep(5)
        self.driver.get('https://getnada.com/')
